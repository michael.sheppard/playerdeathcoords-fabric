/*
 * LastDeathCommand.java
 *
 *  Copyright (c) 2021 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.playerdeathcoords.common;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.entity.Entity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.server.command.CommandManager;

public class LastDeathCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(CommandManager.literal("lastdeath").executes(context -> execute(context.getSource())));
    }

    private static int execute(ServerCommandSource source) {
        BlockPos bp = PlayerDeathCoords.deathPosition;
        Entity entity = source.getEntity();
        if (entity instanceof ServerPlayerEntity) {
            if (bp != null) {
                entity.teleport(bp.getX(), bp.getY(), bp.getZ());
            } else {
                source.sendError(new LiteralText(entity.getEntityName() + " has not died yet!"));
            }
        }
        return 1;
    }
}