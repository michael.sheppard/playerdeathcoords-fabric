/*
 * PlayerDeathMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.playerdeathcoords.mixin;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.Vec3d;
import net.playerdeathcoords.common.PlayerDeathCoords;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(ServerPlayerEntity.class)
public abstract class PlayerDeathMixin {

    @Inject(at = @At(value = "HEAD"), method = "onDeath")
    private void getDeathCoordinates(DamageSource source, CallbackInfo info) {
        PlayerEntity player = (PlayerEntity)(Object)this;
        Vec3d vec = player.getPos();
        String str = String.format("%s stupidly died at: %3.2f/%3.2f/%3.2f", player.getName().asString(), vec.x, vec.y, vec.z);
        PlayerDeathCoords.LOGGER.info(str);
        player.sendMessage(new LiteralText(str), false);
    }
}
